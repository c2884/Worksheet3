#include <iostream>
#include <cstdio>
#include <string>
using std::string;
using std::cout;
using std::cin;
using std::endl;

int main() {
    int num;
    cout << "Please enter the number of times the statement should be printed: \n";
    cin >> num;
    if (!(int)num ){
        printf("Invalid input");
    }
    else{
        for(int i =0; i < num; i++){
            cout << "this statement is repeated \n";
        }
    }
    return 0;
}